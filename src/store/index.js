import Vue from 'vue'
import Vuex from 'vuex'
import selfUser from '../model/SelfUser'

Vue.use(Vuex)

const token = localStorage.getItem('userToken')

export default new Vuex.Store({
  state: {
    token,
    selfUser,
    admin: null
  },
  mutations: {
    setSelfUser (state, { id, name, email, avatarURL }) {
      state.selfUser.id = id
      state.selfUser.name = name
      state.selfUser.email = email
      state.selfUser.avatarURL = avatarURL
    },
    reset (state) {
      state.selfUser.reset()
      state.token = null
      localStorage.removeItem('userToken')
    }
  }
})
