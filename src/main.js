import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-TW'
import App from './App'
import router from './router'
import store from './store'
import api from './api'
import moment from 'moment'
import IsLoadingMixin from './mixins/app-level-is-loading'
import 'bootstrap'

Vue.use(ElementUI, { locale })
Vue.mixin(IsLoadingMixin)

Vue.prototype.$store = store
Vue.prototype.$api = api
Vue.prototype.$moment = moment
Vue.prototype.$bus = new Vue()
Vue.prototype.$errorHandler = function (e) {
  console.error(e)
  const { response: { data: message = e.message } = {} } = e || {}
  if (message) {
    ElementUI.Message.error(message)
  }
}

Vue.config.productionTip = false

Vue.filter('formateDate', momentObject => {
  if (!momentObject) {
    return ''
  } else {
    return moment(momentObject).format('YYYY-MM-DD')
  }
})

Vue.filter('formateDateTime', momentObject => {
  if (!momentObject) {
    return ''
  } else {
    return moment(momentObject).format('YYYY-MM-DD HH:mm:ss')
  }
})

/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  render: h => h(App)
})

// debug
window.store = store
window.moment = moment
window.app = app
