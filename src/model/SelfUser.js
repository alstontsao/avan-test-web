// singleton object
export default new class SelfUser {
  constructor (json = {}) {
    this.id = json.id
    this.name = json.name
    this.email = json.email
    this.avatarURL = json.avatarURL
  }

  reset () {
    this.id = null
    this.name = null
    this.email = null
    this.avatarURL = null
  }
}()
