import axios from 'axios'

const baseURL = 'https://avan-test-api.appccic.com/api/v1'
const instance = axios.create({
  baseURL,
  timeout: 30 * 1000
})

export default {
  baseURL,
  get (url, params) {
    return instance.get(url, { params })
  },
  post: instance.post,
  put: instance.put,
  delete: instance.delete,
  setHeader (dict) {
    Object.keys(dict).forEach(key => {
      instance.defaults.headers.common[key] = dict[key]
    })
  }
}
