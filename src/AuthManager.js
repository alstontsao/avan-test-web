import api from './api'
import store from './store'

const setToken = (token, tokenType = 'Bearer') => {
  if (!token) return
  api.setHeader({
    Authorization: `${tokenType} ${token}`
  })
  store.commit('setUserToken', { token, tokenType })
}

const afterLoginProcess = async (token) => {
  setToken(token)
  const { data: admin } = await api.get('/admin/me')
  store.state.selfUser.loadFromJSON(admin)
  return admin
}

const loginWithStoreToken = async () => {
  const { token } = store.state
  if (!token) return
  await afterLoginProcess(token)
}

const login = (account, password) => {
  return new Promise((resolve, reject) => {
    api.post('/admin/login', { account, password }).then(async ({ data }) => {
      const { token } = data
      await afterLoginProcess(token)
      resolve()
    }).catch(reject)
  })
}

const onGoogleSignInSuccess = (user) => {
  const googleUser = {
    id: user.w3.Eea,
    name: user.w3.ig,
    email: user.w3.U3
  }
  return new Promise((resolve, reject) => {
    api.post('/member/oauth_login/google', googleUser).then(res => {
      const token = res.data.access_token
      const tokenType = res.data.token_type
      afterLoginProcess(token, tokenType, resolve)
    }).catch(reject)
  })
}

const logout = () => {
  store.commit('reset')
  api.setHeader({
    Authorization: ''
  })
}

export default {
  setToken,
  login,
  logout,
  loginWithStoreToken,
  onGoogleSignInSuccess
}
